const express = require('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    data: [
      {
        id: 1,
        name: 'Uciha Udin',
      },
      {
        id: 2,
        name: 'Uzumaki Putin',
      },
    ],
  });
});

app.listen(port, () => console.log(`App run at port: ${port}`));
